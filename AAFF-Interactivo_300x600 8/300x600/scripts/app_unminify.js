function animFirstFrameFn() {

    var TL = new TimelineMax({ paused:true }),
        easeTypes = [
            "Power0.easeNone",
            "Power3.easeOut"
        ];

    var stripes = document.getElementsByClassName("stripe"),
        logoFull = document.querySelector(".logofull"),
        f1Title = document.querySelector(".f1-title"),
        arrows = document.querySelector(".f1-arrows"),
        click = false;

    TL.fromTo(stripes[0], 0.3, {css:{ width:"0%" }}, {css:{ width:"100%" }, ease:easeTypes[0] })
        .fromTo(stripes[1], 0.4, {css:{ height:"0%" }}, {css:{ height:"100%" }, ease:easeTypes[0] })
        .fromTo(stripes[2], 0.2, {css:{ width:"0%" }}, {css:{ width:"100%" }, ease:easeTypes[0] })
        .fromTo(stripes[3], 0.5, {css:{ height:"0%" }}, {css:{ height:"100%" }, ease:easeTypes[2]});

    TL.fromTo(logoFull, 0.8, { opacity:0, yPercent:50 }, { opacity:1, yPercent:0 }, 0.6);
    TL.fromTo(f1Title, 0.8 ,
        { opacity:0, scale:1.1 },
        { opacity:1, scale:1,
            onStart:function(){

                TweenMax.set(arrows, { className:"+=anim" });
                TweenMax.fromTo(arrows, 1.2, { opacity:0 }, { opacity:1 });

                setTimeout(function() {
                    if(click === false) {
                        TweenMax.set(["#frame1", logoFull], { display:"none" });
                        startGame();
                    }

                }, 5000);

                arrows.addEventListener("click", function(){
                    click = true;
                    TweenMax.set(this, { css:{ cursor:"default" } });
                    TweenMax.to([this, logoFull, f1Title], 0.3, {
                        autoAlpha:0,
                        onComplete:function(){

                            TweenMax.set(["#frame1", logoFull], { display:"none" });
                            startGame();

                        }
                    });

                });

            } }, 1.2);

    return TL;


}
var animFirstFrame = animFirstFrameFn();
animFirstFrame.play();
var countingDown = false;
var fadeingIn = false;
var fadeingOut = false;

function startGame() {

    var switchMenu = document.getElementById("switch"),
        frame2Container = document.getElementById("frame2"),
        frame3Container = document.getElementById("frame3");

    var bgImg = document.querySelectorAll(".bg"),
        f2Title = document.querySelector(".f2-title");

    var degreeDisplay = document.querySelector(".degree-display");

    var aLeft = document.getElementById("aleft"), aRight = document.getElementById("aright"),
        symMinus = document.getElementById("symminus"), symPlus = document.getElementById("symplus");

    TweenMax.set(degreeDisplay, { autoAlpha:0 });
    TweenMax.to([switchMenu, frame2Container], 0.6, { autoAlpha:1, onComplete:function(){ TweenMax.set(frame2Container, { className:"-=absolute" }); } });

    var mainTL = new TimelineMax({})
        .fromTo(f2Title, 0.6, { yPercent:50, opacity:0 }, { yPercent:0, opacity:1 })
        .staggerFromTo([aLeft, symMinus, symPlus, aRight], 1,
            { yPercent:-30, opacity:0 },
            {
                yPercent:0,
                opacity:1,
                ease:Elastic.easeOut.config(1, 0.6)
            }, 0.1, 0.3)
        .to(degreeDisplay, 0.6, { autoAlpha:1 }, 0.3)
        .fromTo(degreeDisplay, 1, { yPercent:-30 }, { yPercent:0, ease:Elastic.easeOut.config(1, 0.6) }, 0.3);

    var initNum = { num:0 },
        numDisplay = document.getElementById('num');

    var counter = 5;
    function startCounter() {

        clearInterval(counter);
        countingDown = true;

        setInterval(function() {
            counter--;

            if (counter >= 0) {

                console.log(counter);

            }
            if (counter === 0) {

                animLastFrameFn().play();

            }

        }, 1000);

    }

    setTimeout(function() {
        if(counter == 5) { 
            animLastFrameFn().play();
        }     
    }, 5000);

    function setNum(num) { TweenMax.to(initNum, 0.3,
        {
            num:num, roundProps:"num",
            onUpdate:updateHandler,
            ease:Linear.easeNone
        });
    }

    function addNum() { TweenMax.to(initNum, 0.5,
        {
            num:"+=5,6", roundProps:"num",
            onUpdate:updateHandler,
            ease:Linear.easeNone
        });
    }
    function subNum() { TweenMax.to(initNum, 0.5, {
        num:"-=5,6", 
        roundProps:"num", 
        onUpdate:updateHandler, 
        ease:Linear.easeNone}); 
    }
    function updateHandler() { numDisplay.innerHTML = initNum.num; }

    var active, nextframe, prevframe;

   function fadeOutBgImages() {

        if(!fadeingOut){
            fadeingOut = true;
            active = document.querySelector(".active"), nextframe = active.nextElementSibling;

            var TL = new TimelineMax({ paused:true });

            TL.to(active, 0.3, {
                opacity:0,
                onStart:function() {

                    nextframe.classList.add("nextframe");


                },
                onComplete:function(){

                    active.classList.remove("active");
                    nextframe.classList.add("active");
                    nextframe.classList.remove("nextframe");
                    fadeingOut = false;

                }
            });

            return TL;
        }else{
            return {'play':function(){}};
        }

    }
    function fadeInBgImages() {

        if(!fadeingIn){
            fadeingIn = true;
            active = document.querySelector(".active"), prevframe = active.previousElementSibling;

            var TL = new TimelineMax({ paused:true });

            TL.to(prevframe, 0.3, {
                opacity:1,
                onStart:function(){

                    prevframe.classList.add("prevframe");

                },
                onComplete:function(){

                    active.classList.remove("active");
                    prevframe.classList.add("active");
                    prevframe.classList.remove("prevframe");
                    fadeingIn = false;

                }
            });

            return TL;
        }else{
            return {'play':function(){}};
        }

    }

    function cycleNext() {
        var maxtemp = 20;

        if((initNum.num + 10) <= maxtemp){
            if(bgImg[2].classList.contains("active")) {

                counter = 3;

            } else if(bgImg[0].classList.contains("active") || bgImg[1].classList.contains("active")) {

               counter = 5;

            }

            if(!bgImg[3].classList.contains("active")) {
                if(!countingDown){
                    startCounter();
                }
                fadeOutBgImages().play();
                addNum();
            }
        }else{
            setNum(maxtemp);
        }
    }
    function cyclePrev() {
        var mintemp = 0;
        counter = 5;
        if((initNum.num - 10) >= mintemp){
            if(!bgImg[0].classList.contains("active")) {
                if(!countingDown){
                    startCounter();
                }
                fadeInBgImages().play();
                subNum();
            }
        }else{
            setNum(mintemp);
        }
    }

    aRight.addEventListener("click", cycleNext);
    symPlus.addEventListener("click", cycleNext);

    aLeft.addEventListener("click", cyclePrev);
    symMinus.addEventListener("click", cyclePrev);

    function animLastFrameFn() {

        var logoLite = document.querySelector(".logolite"),
            phoneHand = document.getElementById("phonehand"),
            caldera = document.getElementById("caldera"),
            sello = document.getElementById("sello"),
            sello_last_frame = document.getElementById("sello_last_frame"),
            wifi = document.getElementById("wifi-signal"),
            wifiBars = document.querySelectorAll('.wifibar'),
            wifiSignalBars = [wifiBars[1], wifiBars[2], wifiBars[3]];

        var TL = new TimelineMax({});

        TL.to([frame2Container, switchMenu], 1,
            {
                autoAlpha:0,
                onComplete:function(){
                    TweenMax.set([frame2Container, switchMenu], { css:{ display:'none' }});
                    TweenMax.set(".buttons", {css:{ display:'block'}});
                    TweenMax.set(frame3Container, { className:"-=absolute" });
                }

            }, 0.5)
            .to(frame3Container, 1, { autoAlpha:1 }, 0.5)
            .fromTo(logoLite, 0.8, { opacity:0, yPercent:50 }, { opacity:1, yPercent:0 }, 0.5)
            .fromTo(phoneHand, 2, { yPercent:100 }, { yPercent:0, ease:Power1.easeOut }, 0.7)
            .fromTo(phoneHand, 2, { scale:1 }, { scale:1.1, transformOrigin:"50% 100%", ease:Power1.easeOut }, 1.5);

        TL.to(phoneHand, 1, { scale:0.8, xPercent:20 }, 3.7)
            .to([".lastbg", logoLite], 1, { autoAlpha:0 }, 3.7)
            .to(".subframe:nth-child(1)", 1, { autoAlpha:1 }, 3.7);

        TL.staggerFromTo([caldera, sello], 1, { autoAlpha:0, yPercent:-60 }, { autoAlpha:1, yPercent:0 }, 0.2, 3.7);

        TL.set(wifi, { autoAlpha:1 }, 4.5);

        TL.staggerFromTo(wifiBars, 1, { opacity:0 }, { opacity:1 }, 0.2, 4.5);
        TL.staggerFromTo(wifiSignalBars, 1,
            { opacity:1 },
            { opacity:.2, repeat:-1, yoyo:true }, 0.2, 5.7);

        setTimeout(function(){ 
            sello_last_frame.className = 'effect';
        }, 9500);
        
        
        TL.to([caldera, sello, wifi, phoneHand], 0.6, { autoAlpha:0, onComplete:function(){ TweenMax.killChildTweensOf(wifi); } }, 8)
            .to("#lastclaim", 1, { autoAlpha:1 }, 8.6)
            .fromTo(["#lastclaim p:first-child"], 1, { yPercent:100, opacity:0 }, { yPercent:0, opacity:1 }, 8.6)
            .set(".logofull", {css:{ display:'block' }}, 9.6)
            .fromTo(["#lastclaim p:last-child", ".logofull"], 1, { autoAlpha:0 }, {  autoAlpha:1 }, 9.6);
        
        return TL;

    }
}
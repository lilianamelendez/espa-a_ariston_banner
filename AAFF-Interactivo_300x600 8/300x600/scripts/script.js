var adDiv;

function initEB() {
    if (!EB.isInitialized()) {
        EB.addEventListener(EBG.EventName.EB_INITIALIZED, startAd);
    } else {
        startAd();
    }
}

function startAd() {
    adDiv = document.getElementById("ad");

    addEventListeners();
}

function lessTemperatureCounter(hypeDocument, element, event ) {
    EB.userActionCounter("lesstemperature");
} 

function moreTemperatureCounter(hypeDocument, element, event ) {
    EB.userActionCounter("moretemperature");
} 

function addEventListeners() {
    document.getElementById("clickthrough-button").addEventListener("click", clickthrough);
    document.getElementById("aleft").addEventListener("click", lessTemperatureCounter);
    document.getElementById("aright").addEventListener("click", moreTemperatureCounter);
}

function clickthrough() {
    EB.clickthrough();
}

/*function userActionCounter() {
    EB.userActionCounter("CustomInteraction");
}*/

window.addEventListener("load", initEB);
